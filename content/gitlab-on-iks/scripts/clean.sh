#!/bin/bash
# Written by eggshell - mctaylor@us.ibm.com
#
# This is intended to be used for cleaning up a kubernetes cluster after its
# integration is removed from GitLab. This will allow you to reinstall software
# via GitLab cleanly. Currently, it ensures Tiller, Ingress, Prometheus, and
# Runner are cleaned up after.

function delete_namespaces(){
  kubectl delete namespace gitlab-managed-apps
  kubectl get namespace | grep "spring-app" | cut -d' ' -f1 | xargs kubectl delete
}

function delete_tiller(){
  kubectl delete deploy tiller-deploy -n gitlab-managed-apps
  kubectl delete service tiller-deploy -n gitlab-managed-apps
}

function delete_clusterroles(){
  kubectl delete clusterrole ingress-nginx-ingress
  kubectl delete clusterrole prometheus-kube-state-metrics
  kubectl delete clusterrole prometheus-prometheus-server
}

function delete_clusterrolebindings(){
  kubectl delete clusterrolebinding ingress-nginx-ingress
  kubectl delete clusterrolebinding tiller-admin
  kubectl delete clusterrolebinding prometheus-kube-state-metrics
  kubectl delete clusterrolebinding prometheus-prometheus-server
}

function main(){
  helm delete --purge gitlab

  delete_namespaces
  delete_tiller
  delete_clusterroles
  delete_clusterrolebindings
}

main "@"
