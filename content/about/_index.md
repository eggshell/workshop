---
title: About
subtitle: Who is running this site?
comments: false
---

Hi! My name is JJ Asghar and work in DBG at IBM. You can reach me at <jja@ibm.com>.

If you have any questions or thoughts never hesitate to reach out.

![](https://avatars2.githubusercontent.com/u/810824?s=200&v=4)
